import React from 'react'
import './App.css'
import Counter from './components/counter'
import Jeu from './components/harryP'

function App() {
  return (
    <div className='App'>
      <Counter label='Griffondor'></Counter>
      <Counter label='serpenta'></Counter>
      <Jeu></Jeu>
    </div>
  )
}

export default App
