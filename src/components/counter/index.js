import React, { useState } from 'react'
import PropTypes from 'prop-types'

const Counter = ({ label }) => {
  const [counter, setCounter] = useState(0)
  return (
    <div>
      <p>{label}</p>
      <div>
        <p>{counter}</p>
        <button onClick={() => setCounter(counter - 1)}>-</button>
        <button onClick={() => setCounter(counter + 1)}>+</button>
      </div>
    </div>
  )
}

Counter.propTypes = {
  label: PropTypes.string
}
export default Counter
