import React, { useState } from 'react'
import axios from 'axios'

const Jeu = () => {
  let [users, setUser] = useState([])
  axios.get(`http://hp-api.herokuapp.com/api/characters`).then(res => {
    setUser(res.data)
    console.log(users)
  })
  return (
    <div>
      {users.length > 0 ? (
        <div>
          {users.map(u => (
            <span key={u.name}>{u.name}</span>
          ))}
        </div>
      ) : (
        <></>
      )}
    </div>
  )
}

export default Jeu
